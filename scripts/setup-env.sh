CENO_DIR="${HOME}/ceno"
OUINET_DIR="${CENO_DIR}/ouinet"
BUILD_DIR="${OUINET_DIR}/ouinet-local-build"
GUIX_PROFILE="${HOME}/env/guix/ouinet"
SSH_DIR="${HOME}/.ssh"
mkdir -p ${SSH_DIR}
cd ${SSH_DIR}
ssh-keygen -b 2048 -t rsa -f "${SSH_DIR}/id_rsa" -q -N ""
cd "${HOME}"
sudo apt update
sudo apt install -y git daemonize
sudo service guix-daemon start
git clone --recursive https://gitlab.com/censorship-no/ceno-wsl-client ceno
cd "${OUINET_DIR}"
mkdir -p "${GUIX_PROFILE}"
guix time-machine --channels="${OUINET_DIR}/guix/channels.scm" -- environment -CN --pure --manifest="${OUINET_DIR}/guix/manifest.scm" --expose="${CENO_DIR}"
# TODO: guix shell was not working easily, just use environment for now and manually start ouinet build by running ../scripts/build-ouinet.sh from OUINET_DIR
#guix pull
#guix time-machine --channels="${OUINET_DIR}/guix/channels.scm" -- package --profile="${GUIX_PROFILE}/build" --manifest="${OUINET_DIR}/guix/manifest.scm"
#guix shell -CN --pure --profile="${GUIX_PROFILE}/build" --expose="${OUINET_DIR}" < "${CENO_DIR}/scripts/build-ouinet-guix.sh"
exit