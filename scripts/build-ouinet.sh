CENO_DIR="${HOME}/ceno"
OUINET_DIR="${CENO_DIR}/ouinet"
export TERM=vt100
time env SSL_CERT_FILE= GUIX_LOCPATH=$GUIX_ENVIRONMENT/lib/locale LANG=en_US.UTF-8 bash $OUINET_DIR/scripts/build-ouinet-local.sh
patchelf --set-interpreter "$(patchelf --print-interpreter ouinet-local-build/CMakeFiles/*/CompilerIdCXX/a.out)" ouinet-local-build/golang/bin/go
time env SSL_CERT_FILE= GUIX_LOCPATH=$GUIX_ENVIRONMENT/lib/locale LANG=en_US.UTF-8 bash $OUINET_DIR/scripts/build-ouinet-local.sh
exit