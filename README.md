# CENO Windows Subsystem for Linux (WSL) Client

Installation and configuration scripts for Windows-based clients in the CENO network

See https://censorship.no/.

A CENO client has three main roles:
1. Allowing a user to retrieve potentially censored content from injectors or other users.
2. Sharing retrieved content with other users (seeding).
3. Allowing other users to reach injectors (bridging).

## Getting started

1. Download WSL image and import it with `wsl --import CENO C:\path\to\distro\dir C:\path\to\image\ceno-wsl-client.tar`
2. Execute run.bat
3. Configure your browser to use the CENO network

The Windows computer on which the CENO WSL Client being installed must support Windows Subsystem for Linux, see [Ubuntu on WSL](https://ubuntu.com/wsl) for more information about requirments for WSL.

## Developing
To build the ceno-wsl-client image:
1. Install a clean Ubuntu WSL image with `wsl --install -d ubuntu`
2. Clone this repository and execute `setup.bat`, this will take some time to install the packages requird to build ouinet.
3. When this is done, your terminal will be in the ouinet build environment, execute `../scripts/build-ouinet.sh` to start building ouinet.
4. Export the CENO WSL image with `wsl --export Ubuntu ceno-wsl-client.tar`

# Testing the client with a browser

**Warning:** Browsing via a CENO client on Windows is currently experimental. Please use the CENO Browser on Android for full experience.

  1. Clone the CENO Web Extension repo: `git clone https://github.com/censorship-no/ceno-ext-settings.git`
  2. Create a test profile for Firefox: `mkdir ceno-test`
  3. Run Firefox with the test profile: `firefox --no-remote --profile ceno-test`
  4. Go to <http://localhost:8078/> and install the client-specific CA certificate linked in there to identify web sites.
  5. Enable the CENO Extension: in Firefox's *Add-ons* window, click on the gears icon, then *Debug Add-ons*, then *Load Temporary Add-on…* and choose the *manifest.json* file under the `ceno-ext-settings` directory.
  6. Since CENO Browser v1.3.0 and newer uses HTTPS for all pages, enable *HTTPS-Only Mode* in Firefox's *Settings* window, under *Privacy & Security*. For older browsers lacking that option, install the [HTTPS by default][] extension instead.

**Note:** If you have an old CENO Extension (less than v1.4.0, click on *Manifest URL* to check its `version` close to the beginning), you also need to set `localhost` port `8077` as Firefox's proxy for protocols HTTP and HTTPS/SSL: <https://www.wikihow.com/Enter-Proxy-Settings-in-Firefox>

In subsequent test browser runs you will only need to follow steps 3 and 5.

Browse freely or check the [CENO User Manual][ceno-man-test] for some testing instructions.

[HTTPS by default]: https://addons.mozilla.org/ca/firefox/addon/https-by-default/
[ceno-man-test]: https://censorship.no/user-manual/en/browser/testing.html
    "CENO User Manual - Testing the Browser"
